<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->

**Table of Contents**

[[_TOC_]]

# Istio Service

* [Service Overview](https://dashboards.gitlab.net/d/istio-istio_control_plane/istio-istio-control-plane-dashboard)
* **Alerts**: <https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22istio%22%2C%20tier%3D%22inf%22%7D>
* **Label**: gitlab-com/gl-infra/production~"Service::Istio"

## Logging

* [Istio]()

<!-- END_MARKER -->

<!-- ## Summary -->

<!-- ## Architecture -->

<!-- ## Performance -->

<!-- ## Scalability -->

<!-- ## Availability -->

<!-- ## Durability -->

<!-- ## Security/Compliance -->

<!-- ## Monitoring/Alerting -->

<!-- ## Links to further Documentation -->
